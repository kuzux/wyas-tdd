{-# LANGUAGE DeriveGeneric, OverloadedStrings, StrictData #-}

module Lib where

import Control.Applicative ((<|>))
import Data.List ( intersperse )
import Data.Text ( Text )
import qualified Data.Text as T
import Data.Text.Arbitrary ()
import Debug.Trace ( trace )
import Generic.Random ( genericArbitraryU )
import GHC.Generics
import Test.QuickCheck( Arbitrary(..) )
import Text.Parsec ( ParseError, many, skipMany, try, option, parserFail, sepBy, eof )
import Text.Parsec.Char ( char, oneOf, noneOf, letter, alphaNum, string, space )
import Text.Parsec.Text ( Parser )

-- AstNode is separate from LispVal, which is very much against the spirit of lisp
-- But that is purely because we can generate arbitrary ast s for testing purposes
-- Generating random values is not very possible (or at least fairly difficult)
-- and we transform everything to a LispVal after it is out of the parser
data AstNode = 
      AstAtom Text
    | AstNum Int
    | AstBool Bool
    | AstString Text
    | AstList [AstNode]
    | AstDotted [AstNode]
    deriving (Show, Eq, Generic)

instance Arbitrary AstNode where 
    arbitrary = genericArbitraryU

data LispError = 
      SyntaxError ParseError
    | TypeError Text
    | RuntimError Text

data LispVal =
      LispAtom Text
    | LispNum Int
    | LispBool Bool
    | LispString Text
    -- Maybe convert this representation to the LispNil & LispCons style
    -- all the property based tests used to work with that representation
    | LispNil
    | LispCons LispVal LispVal
    | LispPrimFn { _primName :: T.Text, _primFn :: ([LispVal] -> LispValErr) }
    | LispFn { _fnName :: Maybe T.Text
             , _fnArgs :: [T.Text]
             , _fnBody :: LispVal }
    deriving (Generic)

type LispValErr = Either LispError LispVal

instance Eq LispVal where
    (LispAtom x) == (LispAtom y) = x == y
    (LispNum x) == (LispNum y) = x == y
    (LispBool x) == (LispBool y) = x == y
    (LispString x) == (LispString y) = x == y
    LispNil == LispNil = True
    (LispCons xcar xcdr) == (LispCons ycar ycdr) = (xcar == ycar) && (xcdr == ycdr)
    (LispPrimFn n _ ) == (LispPrimFn m _) = n == m
    (LispFn _ ax bx) == (LispFn _ ay by) = (ax == ay) && (bx == by)
    _ == _ = False

showLispList :: LispVal -> LispVal -> String
showLispList car cdr = inParen . mconcat . (intersperse " ") $ show' car cdr
    where inParen x = "(" <> x <> ")"
          show' x y = case y of
              LispNil -> [show x]
              (LispCons na nd) -> (show x):(show' na nd)
              _ -> [show x, ".", show y]

-- is there a Text.Show kind of typeclass?
instance Show LispVal where
    show (LispAtom x)       = T.unpack x
    show (LispNum x)        = show x
    show (LispBool True)    = "#t"
    show (LispBool False)   = "#f"
    show (LispString x)     = "\"" <> (T.unpack x) <> "\""
    show LispNil            = "nil"
    show (LispCons car cdr) = showLispList car cdr
    show f@(LispPrimFn _ _) = "#<primitive:" <> (T.unpack . _primName $ f) <> ">"
    show f@(LispFn _ _ _)   = "#<primitive:" <> (T.unpack . (maybe "?" id) . _fnName $ f) <> ">"

astToLispList :: [AstNode] -> LispVal
astToLispList xs = foldr LispCons LispNil (toLispVal <$> xs)

dottedToLispList :: [AstNode] -> AstNode -> LispVal
dottedToLispList xs y = foldr LispCons (toLispVal y) (toLispVal <$> xs)

-- ast to unevaluated lisp values
toLispVal :: AstNode -> LispVal
toLispVal (AstAtom x)    = LispAtom x
toLispVal (AstNum x)     = LispNum x 
toLispVal (AstBool x)    = LispBool x
toLispVal (AstString x)  = LispString x
toLispVal (AstList xs)   = astToLispList xs
-- TODO: Make sure we generate "right" dotted lists in quickcheck, to reomve that hack
toLispVal (AstDotted xs) = if length xs < 2 then LispNil else dottedToLispList (init xs) (last xs)

{-
evalExpr :: LispVal -> LispValError
evalExpr = _
-}

atomParser :: Parser AstNode
atomParser = do 
    first <- letter
    rest <- many (alphaNum <|> symbol)
    pure . toLispVal $ first:rest

    where symbol = oneOf "!#$%&|*+-/:<=>?@^_~"
          toLispVal "nil" = AstList []
          toLispVal x = AstAtom . T.pack $ x

boolParser :: Parser AstNode
boolParser = do
    char '#'
    x <- letter
    case x of
        't' -> pure $ AstBool True
        'f' -> pure $ AstBool False
        _   -> parserFail "Invalid boolean value"

numParser :: Parser AstNode
numParser = AstNum . round . read <$> num 
    where num = do
                sign <- option ' ' ((char '-') *> (pure '-'))
                body <- hex <|> decimal
                pure $ (sign:body)
          hex = do 
              string "0x"
              first <- oneOf "123456789abcdef"
              rest <- many (oneOf "0123456789abcdef")
              pure $ "0x" <> (first:rest)
          decimal = do 
              first <- oneOf "123456789"
              rest <- many (oneOf "0123456789")
              expo <- option "e0" (try exponent)
              pure $ (first:rest) <> expo
          exponent = do 
              char 'e'
              first <- oneOf "123456789"
              rest <- many (oneOf "0123456789")
              pure $ 'e':first:rest

dottedParser :: Parser AstNode
dottedParser =  do
    char '('
    e1s <- sepBy partExprParser spaces
    char '.'
    e2 <- partExprParser
    char ')'
    pure $ case e1s of
        [e1] -> AstDotted [e1, e2]
        _    -> AstDotted $ e1s ++ [e2]

    where spaces = skipMany space

listParser :: Parser AstNode
listParser = do
    char '('
    spaces
    elems <- sepBy partExprParser spaces
    spaces
    char ')'
    pure . AstList $ elems

    where spaces = skipMany space 

quotedParser :: Parser AstNode
quotedParser = do 
    char '\''
    e <- partExprParser
    pure $ AstList [AstAtom "quote", e]

stringParser :: Parser AstNode
stringParser = do
    char '"'
    xs <- many (noneOf "\"")
    char '"'
    pure . AstString . T.pack $ xs

isAstList :: AstNode -> Bool
isAstList (AstList _) = True
isAstList _ = False

partExprParser :: Parser AstNode
partExprParser = do
    spaces 
    e <- expr' 
    spaces
    pure e

    where spaces = skipMany space
          expr'  = numParser <|> atomParser <|> boolParser <|> quotedParser <|> stringParser <|> (try dottedParser <|> listParser)

exprParser :: Parser AstNode
exprParser = partExprParser <* eof

-- TODO: handle comments and multiple expressions in that parser
fileParser :: Parser [AstNode]
fileParser = undefined 

-- TODO: read from a file and just run the file parser
parseFile :: FilePath -> IO (Either ParseError [AstNode])
parseFile path = undefined

showExpr :: AstNode -> Text
showExpr = T.pack . show . toLispVal

-- evalExpr should have a type like StateT LispContext (Either LispError LispVal) IO
evalExpr :: LispVal -> LispVal
evalExpr x@(LispNum _) = x
evalExpr x@(LispBool _) = x
evalExpr x@(LispString _) = x
-- lambdas also evaluate to themselves
-- evalExpr x@(LispLambda _) = x
evalExpr LispNil = LispNil
evalExpr (LispCons (LispAtom "quote") (LispCons x xs)) = case xs of
    LispNil -> x
    _       -> error "quote: expected 1 argument, got more"
evalExpr (LispCons (LispAtom "eval") (LispCons x xs)) = case xs of
    LispNil -> evalExpr x
    _       -> error "eval: expected 1 argument, got more"
evalExpr (LispCons (LispAtom "lambda") (LispCons x (LispCons y xs))) = case xs of
    LispNil -> error "TODO: implement lambda construction"
    _       -> error "lambda: expected 2 argument, got more"
evalExpr _ = error "TODO: implement lookups and stuff"

someFunc :: IO ()
someFunc = putStrLn "someFunc"
