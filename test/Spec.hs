{-# LANGUAGE OverloadedStrings, ScopedTypeVariables #-}

import Data.Either ( isLeft )
import Data.Text ( Text )
import qualified Data.Text as T
import Debug.Trace
import Lib
import Test.Hspec
import Test.QuickCheck
import Text.Parsec ( parse ) 

assertNoParse :: Text -> Expectation
assertNoParse x = (parse exprParser "" x) `shouldSatisfy` isLeft

shouldParseTo :: Text -> AstNode -> Expectation
shouldParseTo x y = (parse exprParser "" x) `shouldBe` (Right $ y)

shouldEvaluateTo :: Text -> AstNode -> Expectation
shouldEvaluateTo x y = (evalExpr . toLispVal <$> parse exprParser "" x) `shouldBe` (Right . toLispVal $ y)

shouldEvaluateRepr :: Text -> Text -> Expectation
shouldEvaluateRepr x y  = (T.pack . show . evalExpr . toLispVal <$> parse exprParser "" x) `shouldBe` (Right $ y)

shouldHaveRepresentation :: Text -> Text -> Expectation
shouldHaveRepresentation x y = (showExpr <$> parse exprParser "" x) `shouldBe` (Right $ y)

exprParserSpec = do 
    it "Parses numbers correctly" $ do 
        "42"  `shouldParseTo` (AstNum 42)
        "-42" `shouldParseTo` (AstNum (-42))
    it "Parses exponents correctly" $ do 
        "4e2"  `shouldParseTo` (AstNum 400)
        "-4e2" `shouldParseTo` (AstNum (-400))
    it "Parses hexadecimals correctly" $ do 
        "0x42" `shouldParseTo` (AstNum 66)
        "0x4f" `shouldParseTo` (AstNum 79)
    it "Parses nil correctly" $ do
        "nil" `shouldParseTo` (AstList [])
    it "Parses bools correctly" $ do 
        "#t" `shouldParseTo` (AstBool True)
        "#f" `shouldParseTo` (AstBool False)
    it "Parses atoms correctly" $ do
        "sdfds" `shouldParseTo` (AstAtom "sdfds")
        "sdf34ds" `shouldParseTo` (AstAtom "sdf34ds")
        "sdf-ds?" `shouldParseTo` (AstAtom "sdf-ds?")
    it "Parses atom that starts with nil correctly" $ do 
        "nils" `shouldParseTo` (AstAtom "nils")
    it "Fails to parse invalid atoms" $ do
        assertNoParse $ "#sdfds"
        assertNoParse $ "#s"
    it "Fails to parse invalid numbers" $ do
        assertNoParse $ "4sdfds2"
        assertNoParse $ "4e-2"
        assertNoParse $ "0x4g"
    it "Handles leading/trailing spaces in atoms correctly" $ do 
        "  sdfds"     `shouldParseTo` (AstAtom "sdfds")
        "sdfds   "    `shouldParseTo` (AstAtom "sdfds")
        "  sdfds  "   `shouldParseTo` (AstAtom "sdfds")
        " \tsdfds \t" `shouldParseTo` (AstAtom "sdfds")
    it "Parses dotted lists correctly" $ do 
        "(asd . fgh)"     `shouldParseTo` (AstDotted [AstAtom "asd", AstAtom "fgh"])
        "(asd . 42)"      `shouldParseTo` (AstDotted [AstAtom "asd", AstNum 42])
        "(asd   .   fgh)" `shouldParseTo` (AstDotted [AstAtom "asd", AstAtom "fgh"])
        "  (asd . fgh)  " `shouldParseTo` (AstDotted [AstAtom "asd", AstAtom "fgh"])
    it "Fails to parse invalid dotted lists" $ do 
        assertNoParse $ "(asd . fgh jkl)"
        assertNoParse $ "(asd . fgh . jkl)"
    it "Parses empty lists correctly" $ do 
        "nil"  `shouldParseTo` (AstList [])
        "()"   `shouldParseTo` (AstList [])
        "(  )" `shouldParseTo` (AstList [])
    it "Parses non-empty lists correctly" $ do 
        "(asd)"         `shouldParseTo` (AstList [AstAtom "asd"])
        "( asd )"       `shouldParseTo` (AstList [AstAtom "asd"])
        "(asd qwe)"     `shouldParseTo` (AstList [AstAtom "asd", AstAtom "qwe"])
        "(asd 42)"      `shouldParseTo` (AstList [AstAtom "asd", AstNum 42])
        "( asd    qwe)" `shouldParseTo` (AstList [AstAtom "asd", AstAtom "qwe"])
    it "Parses nested lists correctly" $ do
        "(asd (qwe))"             `shouldParseTo` (AstList [AstAtom "asd", (AstList [AstAtom "qwe"])])
        "(asd (qwe (zxc) (mnf)))" `shouldParseTo` (AstList [AstAtom "asd", AstList [AstAtom "qwe", AstList [AstAtom "zxc"], AstList [AstAtom "mnf"]]])
    it "Fails on unbalanced parens" $ do 
        -- TODO: Write a property for generating unbalanced parens
        assertNoParse $ "(asd qwe"
        assertNoParse $ "(asd qwe))"
        assertNoParse $ "(asd (qwe zxc)"
    it "Parses dotted lists with many elements" $ do
        "(asd qwe . zxc)"         `shouldParseTo` (AstDotted [AstAtom "asd", AstAtom "qwe", AstAtom "zxc"])
        "( asd   qwe    .   zxc)" `shouldParseTo` (AstDotted [AstAtom "asd", AstAtom "qwe", AstAtom "zxc"])
    it "Parses quoted atoms correctly" $ do 
        "'asd" `shouldParseTo` (AstList [AstAtom "quote", AstAtom "asd"])
        "'qwe" `shouldParseTo` (AstList [AstAtom "quote", AstAtom "qwe"])
    it "Parses quoted lists correctly" $ do 
        "'(asd)"     `shouldParseTo` (AstList [AstAtom "quote", AstList [AstAtom "asd"]])
        "'(asd qwe)" `shouldParseTo` (AstList [AstAtom "quote", AstList [AstAtom "asd", AstAtom "qwe"]])
    it "Parses strings correctly" $ do 
        "\"sdfds\""   `shouldParseTo` (AstString "sdfds")
        "\"sdf34ds\"" `shouldParseTo` (AstString "sdf34ds")
        "\"sdf-ds?\"" `shouldParseTo` (AstString "sdf-ds?")
    it "Parses strings with escape sequences" $ do 
        pending
    it "Fails to parse invalid strings" $ do
        pending
    it "Parses vectors correctly" $ do
        pending
    it "Parses backquotes correctly" $ do
        pending

showSpec = do
    it "Represents atoms correctly" $ do 
        "qweqwe"    `shouldHaveRepresentation` "qweqwe"
        "asd->fgh?" `shouldHaveRepresentation` "asd->fgh?"
    it "Represents numbers correctly" $ do
        "42"   `shouldHaveRepresentation` "42"
        "-42"  `shouldHaveRepresentation` "-42"
        "0x42" `shouldHaveRepresentation` "66"
        "4e1"  `shouldHaveRepresentation` "40"
    it "Represents booleans correctly" $ do
        "#t" `shouldHaveRepresentation` "#t"
        "#f" `shouldHaveRepresentation` "#f"
    it "Represents nil correctly" $ do
        "nil" `shouldHaveRepresentation` "nil"
        "()"  `shouldHaveRepresentation` "nil"
    it "Represents strings correctly" $ do
        "\"sdf43-ds?\"" `shouldHaveRepresentation` "\"sdf43-ds?\""
    it "Represents escape characters correctly" $ do
        pending
    it "Represents dotted pairs correctly" $ do
        "(asd . qwe)"      `shouldHaveRepresentation` "(asd . qwe)"
        "(asd   .    qwe)" `shouldHaveRepresentation` "(asd . qwe)"
        "(asd . 123)"      `shouldHaveRepresentation` "(asd . 123)"
        "(asd .())"        `shouldHaveRepresentation` "(asd)"
    it "Represents longer dotted lists correctly" $ do
        "(asd sdf . qwe)" `shouldHaveRepresentation` "(asd sdf . qwe)"
        "(asd 123 . qwe)" `shouldHaveRepresentation` "(asd 123 . qwe)"
    it "Represents lists correctly" $ do
        "(asd qwe)"       `shouldHaveRepresentation` "(asd qwe)"
        "(asd   123 qwe)" `shouldHaveRepresentation` "(asd 123 qwe)"
    it "Represents nested lists correctly" $ do
        "(asd (qwe (zxc)) fcfc)" `shouldHaveRepresentation` "(asd (qwe (zxc)) fcfc)"
    it "Represents quoted expressions correctly" $ do
        "'asd"       `shouldHaveRepresentation` "(quote asd)"
        "'(asd qwe)" `shouldHaveRepresentation` "(quote (asd qwe))"
    it "Represents backquoted expressions correctly" $ do
        pending

selfEvalSpec = do
    it "Evaluates numbers to themselves (examples)" $ do
        "42"   `shouldEvaluateRepr` "42"
        "-42"  `shouldEvaluateRepr` "-42"
        "0x42" `shouldEvaluateRepr` "66"
        "4e2"  `shouldEvaluateRepr` "400"
    it "Evaluates numbers to themselves" $ property $ do
        x <- arbitrary
        let xval = LispNum x
        pure $ (evalExpr xval) == xval
    it "Evaluates booleans to themselves" $ do
        "#t" `shouldEvaluateRepr` "#t"
        "#f" `shouldEvaluateRepr` "#f"
    it "Evaluates strings to themselves (examples)" $ do
        "\"sdf43-ds?\"" `shouldEvaluateRepr` "\"sdf43-ds?\""
    it "Evaluates strings to themselves" $ property $ do
        xval <- LispString <$> arbitrary
        pure $ (evalExpr xval) == xval
    it "Evaluates quoted atoms to themselves (examples)" $ do
        "'qweqwe"    `shouldEvaluateRepr` "qweqwe"
        "'asd->fgh?" `shouldEvaluateRepr` "asd->fgh?"
    it "Evaluates quoted atoms to themselves" $ property $ do
        xval <- AstAtom <$> arbitrary
        let quoted = toLispVal $ AstList [AstAtom "quote", xval]
        pure $ (evalExpr quoted) == (toLispVal xval)
    it "Evaluates quoted lists to themselves (examples)" $ do
        "'(asd qwe)"      `shouldEvaluateRepr` "(asd qwe)"
        "'(asd  123 qwe)" `shouldEvaluateRepr` "(asd 123 qwe)"
    {- it "Evaluates quoted lists to themselves" $ property $ do 
        -- I have no idea why those properties seem to just hang
        -- It might be because just checking for equality takes way too long
        -- At least the example based tests work
        xval <- AstList <$> arbitrary
        let quoted = toLispVal $ AstList [AstAtom "quote", xval]
        pure $ (evalExpr quoted) == (toLispVal xval) -}
    it "Evaluates quoted pairs to themselves (examples)" $ do
        "'(3 . 5)"     `shouldEvaluateRepr` "(3 . 5)"
        "'(asd . qwe)" `shouldEvaluateRepr` "(asd . qwe)"
        "'(3 4 . 5)"   `shouldEvaluateRepr` "(3 4 . 5)"
    {-it "Evaluates quoted pairs to themselves" $ property $ do
        x <- arbitrary
        y <- arbitrary
        let expr = AstDotted [x, y]
        let quoted = AstList [AstAtom "quote", expr]
        pure $ (evalExpr quoted) == expr-}
    it "Evaluates empty list to itself" $ do
        "nil" `shouldEvaluateRepr` "nil"
        "()"  `shouldEvaluateRepr` "nil"
    -- note to self: a dotted pair does not evaluate to itself
    
main :: IO ()
main = hspec $ do 
    describe "Expression Parser" exprParserSpec
    describe "Showing Expressions" showSpec
    describe "Values that evaluate to themselves" selfEvalSpec
